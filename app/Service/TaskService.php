<?php

namespace App\Service;

use App\Repositories\StatusTaskRepository;
use App\Repositories\TaskRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class TaskService
{
    /**
     * @var TaskRepository
     */
    public TaskRepository $taskRepository;

    /**
     * @var StatusTaskRepository
     */
    public StatusTaskRepository $statusTaskRepository;

    /**
     * TaskService constructor.
     * @param TaskRepository $taskRepository
     * @param StatusTaskRepository $statusTaskRepository
     */
    public function __construct(
        TaskRepository $taskRepository,
        StatusTaskRepository $statusTaskRepository
    ) {
        $this->taskRepository = $taskRepository;
        $this->statusTaskRepository = $statusTaskRepository;
    }

    /**
     * Get all tasks for project
     * @param array $data
     * @return Collection
     */
    public function getAllTasks(array $data): Collection
    {
        if (isset($data['project_id'])) {
            return $this
                ->taskRepository
                ->allTasks($data['project_id']);
        }

        return $this
            ->taskRepository
            ->all();
    }

    /**
     * Create new task
     * @param array $data
     * @return Model
     */
    public function createNewTask(array $data): Model
    {
        return $this
            ->taskRepository
            ->store($data);
    }

    /**
     * Get one record task by id
     * @param int $id
     * @return Model
     */
    public function getOneTask(int $id): Model
    {
        return $this
            ->taskRepository
            ->getRecord($id);
    }

    /**
     * Update one task by id
     * @param array $data
     * @param int $id
     * @return Model
     */
    public function updateTask(array $data, int $id): Model
    {
        if (isset($data['status'])) {
            $status = $this
                ->statusTaskRepository
                ->getRecordByStatus($data['status']);
        }

        $data['status_id'] = $status->id;

        return $this
            ->taskRepository
            ->update($data, $id);
    }

    /**
     * Delete one record task by id
     * @param int $id
     * @return bool
     */
    public function deleteTask(int $id): bool
    {
        return $this
            ->taskRepository
            ->destroy($id);
    }

    /**
     * Get all task with paginate
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function getAllTasksWithPagination(array $data): LengthAwarePaginator
    {
        $query = $this
            ->taskRepository
            ->newQuery();

        if (isset($data['project'])) {
            $query = $this
                ->taskRepository
                ->whereProject($query, $data['project']);
        }

        if (isset($data['query']) && $data['ascending'] == '1') {
            $query = $this
                ->taskRepository
                ->whereTitle($query, $data['query']);
        }

        if (isset($data['status'])) {
            $query = $this
                ->taskRepository
                ->whereStatus($query, $data['status']);
        }

        if (isset($data['date'])) {
            $query = $this
                ->taskRepository
                ->whereDate($query, $data['date']);
        }

        return $this
            ->taskRepository
            ->queryPaginate($query, $data['limit']);
    }
}
