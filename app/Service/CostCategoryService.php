<?php

namespace App\Service;

use App\Repositories\CostCategoryRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class CostCategoryService
{
    /**
     * @var CostCategoryRepository
     */
    public CostCategoryRepository $costCategoryRepository;

    /**
     * CostCategoryService constructor.
     * @param CostCategoryRepository $costCategoryRepository
     */
    public function __construct(
        CostCategoryRepository $costCategoryRepository
    ) {
        $this->costCategoryRepository = $costCategoryRepository;
    }

    /**
     * Get all categories
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function getAllCategories(int $limit): LengthAwarePaginator
    {
        return $this
            ->costCategoryRepository
            ->paginateAll($limit);
    }

    /**
     * Get one record by id
     * @param int $id
     * @return Model
     */
    public function getOneCostCategory(int $id): Model
    {
        return $this
            ->costCategoryRepository
            ->getRecord($id);
    }

    /**
     * Create new cost category
     * @param array $data
     * @return Model
     */
    public function createNewCostCategory(array $data): Model
    {
        return $this
            ->costCategoryRepository
            ->store($data);
    }

    /**
     * Update one cost category by id
     * @param array $data
     * @param int $id
     * @return Model
     */
    public function updateOneCostCategoryById(array $data, int $id): Model
    {
        return $this
            ->costCategoryRepository
            ->update($data, $id);
    }

    /**
     * Delete one cost category by id
     * @param int $id
     * @return bool
     */
    public function deleteOneCostCategoryById(int $id): bool
    {
        return $this
            ->costCategoryRepository
            ->destroy($id);
    }
}
