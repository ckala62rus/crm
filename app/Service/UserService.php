<?php

namespace App\Service;

use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class UserService
{
    /**
     * @var UserRepository
     */
    public UserRepository $userRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * Get users collection
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function getAllUsers(int $limit): LengthAwarePaginator
    {
        return $this
            ->userRepository
            ->paginateAll($limit);
    }

    /**
     * Get one record with user
     * @param int $id
     * @return Model
     */
    public function getOneRecord(int $id): Model
    {
        return $this
            ->userRepository
            ->getRecord($id);
    }

    /**
     * Create new user
     * @param array $data
     * @return Model
     */
    public function createNewUser(array $data): Model
    {
        return $this
            ->userRepository
            ->store($data);
    }

    /**
     * Destroy oune user by id
     * @param int $id
     * @return bool
     */
    public function destroyOneUser(int $id): bool
    {
        return $this
            ->userRepository
            ->destroy($id);
    }

    /**
     * Update one user
     * @param array $data
     * @param int $id
     * @return Model
     */
    public function updateUser(array $data, int $id): Model
    {
        return $this
            ->userRepository
            ->update($data, $id);
    }
}
