<?php

namespace App\Service;

use App\Repositories\CostRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use PhpParser\ErrorHandler\Collecting;

class CostService
{
    /**
     * @var CostRepository
     */
    public CostRepository $costRepository;

    /**
     * CostService constructor.
     * @param CostRepository $costRepository
     */
    public function __construct(
        CostRepository $costRepository
    ) {
        $this->costRepository = $costRepository;
    }

    /**
     * Get all costs record with paginate
     * @param array $data
     * @return Collection
     */
    public function getAllCosts(array $data): Collection
    {
        $query = $this
            ->costRepository
            ->newQuery();

        if (isset($data['date_from']) && isset($data['date_to'])) {
            $query = $this
                ->costRepository
                ->whereDate($query, $data);
        }

        if (isset($data['category'])) {
            $query = $this
                ->costRepository
                ->whereCategory($query, $data);
        }

        $query = $this
            ->costRepository
            ->sort($query);

        return $query->get();
    }

    /**
     * Get one cost record by id
     * @param int $id
     * @return Model
     */
    public function getOneCostById(int $id): Model
    {
        return $this
            ->costRepository
            ->getRecord($id);
    }

    /**
     * Create new cost
     * @param array $data
     * @return Model
     */
    public function createNewCost(array $data): Model
    {
        return $this
            ->costRepository
            ->store($data);
    }

    /**
     * Update one cost record by id
     * @param array $data
     * @param int $id
     * @return Model
     */
    public function updateOneCost(array $data, int $id): Model
    {
        return $this
            ->costRepository
            ->update($data, $id);
    }

    /**
     * Delete one cost record by id
     * @param int $id
     * @return bool
     */
    public function deleteOneCost(int $id): bool
    {
        return $this
            ->costRepository
            ->destroy($id);
    }

    /**
     * Get sum costs
     * @param object $costs
     * @return float
     */
    public function calculateStatistic(object $costs): float
    {
        $allSum = 0;

        foreach ($costs as $cost) {
            $allSum += $cost->price;
        }

        return $allSum;
    }
}
