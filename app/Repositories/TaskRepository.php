<?php

namespace App\Repositories;

use App\Models\Task;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class TaskRepository extends Repository
{
    /**
     * TaskRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Task();
    }

    /**
     * Get all task for project
     * @param int $projectId
     * @return Collection
     */
    public function allTasks(int $projectId): Collection
    {
        return $this
            ->model
            ->newQuery()
            ->where('project_id', $projectId)
            ->with('status')
            ->get();
    }

    /**
     * Get new query
     * @return Builder
     */
    public function newQuery(): Builder
    {
        return $this
            ->model
            ->newQuery();
    }

    /**
     * Filter where project_id
     * @param Builder $query
     * @param int $project_id
     * @return Builder
     */
    public function whereProject(Builder $query, int $project_id): Builder
    {
        return $query
            ->where('project_id', $project_id);
    }

    /**
     * Search for title
     * @param Builder $query
     * @param string $text
     * @return Builder
     */
    public function whereTitle(Builder $query, string $text): Builder
    {
        return $query
            ->where('title', 'like', '%' . $text . '%');
    }

    /**
     * Search where status
     * @param Builder $query
     * @param $status_id
     * @return Builder
     */
    public function whereStatus(Builder $query, $status_id): Builder
    {
        return $query
            ->where('status_id', $status_id);
    }

    /**
     * Search for date create
     * @param Builder $query
     * @param $date
     * @return Builder
     */
    public function whereDate(Builder $query, $date): Builder
    {
        return $query
            ->where('created_at', 'like', '%' . $date . '%');
    }

    /**
     * Get paginate
     * @param Builder $query
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function queryPaginate(Builder $query, int $limit): LengthAwarePaginator
    {
        return $query
            ->paginate($limit);
    }
}
