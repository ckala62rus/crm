<?php

namespace App\Repositories;

use App\Models\Role;
use Illuminate\Database\Eloquent\Model;

class RoleRepository extends Repository
{
    /**
     * RoleRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Role();
    }

    /**
     * Create role
     * @param array $data
     * @return Model
     */
    public function createRole(array $data): Model
    {
        return $this
            ->model
            ->create($data);
    }
}
