<?php

namespace App\Repositories;

use App\Models\CostCategory;

class CostCategoryRepository extends Repository
{
    /**
     * CostCategoryRepository constructor.
     */
    public function __construct()
    {
        $this->model = new CostCategory();
    }
}
