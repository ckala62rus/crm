<?php

namespace App\Repositories;

use App\Models\StatusTask;
use Illuminate\Database\Eloquent\Model;

class StatusTaskRepository extends Repository
{
    /**
     * StatusTaskRepository constructor.
     */
    public function __construct()
    {
        $this->model = new StatusTask();
    }

    /**
     * Get one status record by status
     * @param string $status
     * @return Model
     */
    public function getRecordByStatus(string $status): Model
    {
        return $this
            ->model()
            ->newQuery()
            ->where('status', $status)
            ->first();
    }
}
