<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Cost extends Model
{
    use HasFactory;

    protected $fillable = [
        'price',
        'category_id',
        'description',
        'date',
    ];

    /**
     * @return HasOne
     */
    public function category(): HasOne
    {
        return $this->hasOne(CostCategory::class, 'id', 'category_id');
    }
}
