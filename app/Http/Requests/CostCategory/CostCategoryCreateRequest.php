<?php

namespace App\Http\Requests\CostCategory;

use Illuminate\Foundation\Http\FormRequest;

class CostCategoryCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => 'required|string|unique:cost_categories',
            'description' => 'string|nullable'
        ];
    }
}
