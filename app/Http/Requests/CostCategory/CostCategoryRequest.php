<?php

namespace App\Http\Requests\CostCategory;

use Illuminate\Foundation\Http\FormRequest;

class CostCategoryRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'limit' => 'required|integer',
        ];
    }
}
