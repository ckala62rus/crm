<?php

namespace App\Http\Requests\PermissionRole;

use Illuminate\Foundation\Http\FormRequest;

class AttachOrDetachRoleUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|int',
            'role_id' => 'required|int',
        ];
    }
}
