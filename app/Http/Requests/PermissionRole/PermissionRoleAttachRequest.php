<?php

namespace App\Http\Requests\PermissionRole;

use Illuminate\Foundation\Http\FormRequest;

class PermissionRoleAttachRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role_id' => 'required|integer',
            'permission_ids' => 'required|array',
        ];
    }
}
