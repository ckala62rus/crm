<?php

namespace App\Http\Controllers;

use App\Http\Requests\Permission\PermissionCreateRequest;
use App\Http\Requests\Permission\PermissionIndexRequest;
use App\Http\Resources\Permission\PermissionCreateResource;
use App\Service\PermissionService;
use Illuminate\Http\JsonResponse;

class PermissionController extends Controller
{
    /**
     * @var PermissionService
     */
    public PermissionService $permissionService;

    /**
     * PermissionController constructor.
     * @param PermissionService $permissionService
     */
    public function __construct(
        PermissionService $permissionService
    ) {
        $this->permissionService = $permissionService;
    }

    /**
     * Get all permissions
     * @param PermissionIndexRequest $request
     * @return JsonResponse
     */
    public function index(PermissionIndexRequest $request): JsonResponse
    {
        $limit = $request->get('limit');

        $permissions = $this
            ->permissionService
            ->getAllPermission($limit);

        return response()->json([
            'data' => PermissionCreateResource::collection($permissions),
            'count' => $permissions->total(),
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Create new permission
     * @param PermissionCreateRequest $request
     * @return JsonResponse
     */
    public function store(PermissionCreateRequest $request): JsonResponse
    {
        $data = $request->all();

        $permission = $this
            ->permissionService
            ->createPermission($data);

        return response()->json(
            new PermissionCreateResource($permission),
            JsonResponse::HTTP_CREATED
        );
    }

    /**
     * Get one permission by id
     * @param int $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        $permission = $this
            ->permissionService
            ->getOnePermission($id);

        return response()->json(
            new PermissionCreateResource($permission),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Update permission by id
     * @param PermissionCreateRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(PermissionCreateRequest $request, $id): JsonResponse
    {
        $data = $request->all();

        $permission = $this
            ->permissionService
            ->updatePermission($data, $id);

        return response()->json(
            new PermissionCreateResource($permission),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Destroy one record
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $data = $this
            ->permissionService
            ->destroyOnePermission($id);

        return response()->json($data, JsonResponse::HTTP_OK);
    }
}
