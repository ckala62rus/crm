<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CostCategory\CostCategoryCreateRequest;
use App\Http\Requests\CostCategory\CostCategoryRequest;
use App\Http\Resources\CostCategory\CostCategoryResource;
use App\Service\CostCategoryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CostCategoryController extends Controller
{
    /**
     * @var CostCategoryService
     */
    public CostCategoryService $costCategoryService;

    /**
     * CostCategoryController constructor.
     * @param CostCategoryService $costCategoryService
     */
    public function __construct(
        CostCategoryService $costCategoryService
    ) {
        $this->costCategoryService = $costCategoryService;
    }

    /**
     * Get all cost categories
     * @param CostCategoryRequest $request
     * @return JsonResponse
     */
    public function index(CostCategoryRequest $request): JsonResponse
    {
        $limit = $request->get('limit');

        $categories = $this
            ->costCategoryService
            ->getAllCategories($limit);

        return response()->json([
            'data' => CostCategoryResource::collection($categories),
            'count' => $categories->total()
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Create one cost category
     * @param CostCategoryCreateRequest $request
     * @return JsonResponse+
     */
    public function store(CostCategoryCreateRequest $request): JsonResponse
    {
        $data = $request->all();

        $category = $this
            ->costCategoryService
            ->createNewCostCategory($data);

        return response()->json(
            new CostCategoryResource($category),
            JsonResponse::HTTP_CREATED
        );
    }

    /**
     * Get one cost category record by id
     * @param $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        $category = $this
            ->costCategoryService
            ->getOneCostCategory($id);

        return response()->json(
            new CostCategoryResource($category),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Update one cost category record by id
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, $id): JsonResponse
    {
        $data = $request->all();

        $category = $this
            ->costCategoryService
            ->updateOneCostCategoryById($data, $id);

        return response()->json(
            new CostCategoryResource($category),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Delete one cost category record by id
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $data = $this
            ->costCategoryService
            ->deleteOneCostCategoryById($id);

        return response()->json($data, JsonResponse::HTTP_OK);
    }
}
