<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Project\ProjectRequest;
use App\Http\Resources\Project\ProjectResource;
use App\Service\ProjectService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * @var ProjectService
     */
    public ProjectService $projectService;

    /**
     * ProjectController constructor.
     * @param ProjectService $projectService
     */
    public function __construct(
        ProjectService $projectService
    ) {
        $this->projectService = $projectService;
    }

    /**
     * Get all projects
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $projects = $this
            ->projectService
            ->getAllProjects();

        return response()->json([
            'data' => ProjectResource::collection($projects),
            'count' => count($projects),
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Create new project
     * @param ProjectRequest $request
     * @return JsonResponse
     */
    public function store(ProjectRequest $request)
    {
        $data = $request->all();

        $project = $this
            ->projectService
            ->createProject($data);

        return response()->json(
            new ProjectResource($project),
            JsonResponse::HTTP_CREATED
        );
    }

    /**
     * Get one project
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $project = $this
            ->projectService
            ->getOneProject($id);

        return response()->json(
            new ProjectResource($project),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Update one project by id
     * @param ProjectRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(ProjectRequest $request, int $id): JsonResponse
    {
        $data = $request->all();

        $project = $this
            ->projectService
            ->updateProjectById($data, $id);

        return response()->json(
            new ProjectResource($project),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Delete project by id
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $data = $this
            ->projectService
            ->deleteProject($id);

        return response()->json($data, JsonResponse::HTTP_OK);
    }
}
