<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Cost\CostCreateRequest;
use App\Http\Requests\Cost\CostRequest;
use App\Http\Requests\Cost\CostUpdateRequest;
use App\Http\Resources\Cost\CostResource;
use App\Service\CostService;
use Illuminate\Http\JsonResponse;

class CostController extends Controller
{
    /**
     * @var CostService
     */
    public CostService $costService;

    /**
     * CostController constructor.
     * @param CostService $costService
     */
    public function __construct(
        CostService $costService
    ) {
        $this->costService = $costService;
    }

    /**
     * Get all costs record
     * @param CostRequest $request
     * @return JsonResponse
     */
    public function index(CostRequest $request): JsonResponse
    {
        $data = $request->all();

        $costs = $this
            ->costService
            ->getAllCosts($data);

        $allSum = $this
            ->costService
            ->calculateStatistic($costs);

        return response()->json([
            'data' => CostResource::collection($costs),
//            'count' => $costs->total(),
            'count' => count($costs),
            'sum' => $allSum,
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Create new cost
     * @param CostCreateRequest $request
     * @return JsonResponse
     */
    public function store(CostCreateRequest $request): JsonResponse
    {
        $data = $request->all();

        $cost = $this
            ->costService
            ->createNewCost($data);

        return response()->json(
            new CostResource($cost),
            JsonResponse::HTTP_CREATED
        );
    }

    /**
     * Get one cost record by id
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $cost = $this
           ->costService
           ->getOneCostById($id);

        return response()->json(
            new CostResource($cost),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Update one cost by id
     * @param CostUpdateRequest $request
     * @param $id
     * @return JsonResponse
     */
    public function update(CostUpdateRequest $request, $id): JsonResponse
    {
        $data = $request->all();

        $cost = $this
            ->costService
            ->updateOneCost($data, $id);

        return response()->json(
            new CostResource($cost),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Delete one cost record by id
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $data = $this
            ->costService
            ->deleteOneCost($id);

        return response()->json($data, JsonResponse::HTTP_OK);
    }
}
