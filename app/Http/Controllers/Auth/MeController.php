<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\Auth\MeResource;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MeController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function me(): JsonResponse
    {
//        $user = Auth::user();

//        return response()->json(new MeResource($user), JsonResponse::HTTP_OK);

        /** @var \Illuminate\Foundation\Auth\User $user */
        $user = auth()->user();

        $user->load([
            "roles",
            "permissions",
        ]);

        return response()->json([
            "user" => $user,
        ]);
    }

    public function testAuth()
    {
        return response()->json([
            'status' => 'OK',
        ]);
    }

//    public function __invoke(Request $request)
//    {
//        /** @var \Illuminate\Foundation\Auth\User $user */
//        $user = auth()->user();
//
//        $user->load([
//            "roles",
//            "permissions",
//        ]);
//
//        return response()->json([
//            "user" => $user,
//        ]);
//    }
}
