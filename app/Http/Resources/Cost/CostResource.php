<?php

namespace App\Http\Resources\Cost;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed id
 * @property mixed price
 * @property mixed category_id
 * @property mixed created_at
 * @property mixed description
 * @property mixed category
 * @property mixed date
 */
class CostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'price' => $this->price,
            'description' => $this->description,
            'category' => $this->category->slug,
            'date' => $this->date,
            'created_at' => Carbon::parse($this->created_at)->format('Y-m-d'),
        ];
    }
}
