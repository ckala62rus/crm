<?php

namespace Database\Seeders;

use App\Models\StatusTask;
use Illuminate\Database\Seeder;

class StatusTaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            'on-hold',
            'in-progress',
            'needs-review',
            'approved',
        ];

        foreach ($statuses as $status) {
            $statusModel = new StatusTask();
            $statusModel->status = $status;
            $statusModel->save();
        }
    }
}
